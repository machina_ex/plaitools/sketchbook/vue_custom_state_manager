
import { createApp, reactive } from 'vue';
console.log(reactive);

import App from './App.vue';
import { stateSymbol, createState } from './store';

const app = createApp(App);
app.provide(stateSymbol, createState());
app.mount('#app');