# vue_custom_state_manager

- all components can read and write the same reactive store.
- minimal boilerplate:

```js
import { useState } from '../store';

export default {
  setup(){
    return { state: useState() };
  }
}
```

makes `state` globally reachable from component.

possible variation: make `state` only readable from component and use events to write.

heavily based on: https://dev.to/blacksonic/you-might-not-need-vuex-with-vue-3-52e4


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
